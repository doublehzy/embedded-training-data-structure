/*************************************************************************
> File Name: main.c
> Author: hzy
> Created Time: 2023-08-02 19:08:11
> Description:  类似于有头链表
************************************************************************/
#include "linkstack.h"

int main(int argc, char const *argv[])
{
    node_t *top = CreateLinkStack();

    for (int i = 1; i <= 5; i++)
    {
        PushLinkStack(top, i);
    }
    DisplayLinkStack(top);
    int len = LenthLinkList(top);
    printf("栈的长度为：%d\n",len);
    PushLinkStack(top,69);
    DisplayLinkStack(top);
    len = LenthLinkList(top);
    printf("栈的长度为：%d\n",len);

    data_t mydata = PopLinkStack(top);
    printf("%d ---> 已出栈 \n",mydata);
    DisplayLinkStack(top);

    return 0;
}
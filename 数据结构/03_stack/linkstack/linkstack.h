/*************************************************************************
> File Name: linkstack.h
> Author: hzy
> Created Time: 2023-08-02 19:08:05
> Description:
************************************************************************/
#ifndef _LINKSTACK_H_
#define _LINKSTACK_H_

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

typedef int data_t;

typedef struct node
{
    data_t data;
    struct node *next;
} node_t;

node_t *CreateLinkStack(void);
int isEmptyLinkStack(node_t *top);
int PushLinkStack(node_t *top, data_t mydata); // 入栈
data_t PopLinkStack(node_t *top);              // 出栈
int LenthLinkList(node_t *top);
int DisplayLinkStack(node_t *top);

#endif
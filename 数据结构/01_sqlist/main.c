/*************************************************************************
> File Name: main.c
> Author: hzy
> Created Time: 2023-07-25 16:28:03
> Description:顺序表的主函数
************************************************************************/
#include "config.h"

int main(int argc, char const *argv[])
{
    int i = 0;
    data_t mydata;

    sqList *list = CreateSqList();
    if (list == NULL)
    {
        printf("创建空间失败");
        return -1;
    }
    else
    {
        printf("创建空间成功\n");
    }

    printf("插入操作:\n");
    i = 5;
    while (--i)
    {
        printf("位置%-2d插入的数据:%-4d\n", InserSqList(list, 1, i), i);
    }
    // 1 2 3 4
    DisplaySqList(list);
    i = 8;
    while (--i)
    {
        printf("位置%-2d插入的数据:%-4d\n", InserSqList(list, 3, i), i);
    }
    DisplaySqList(list);
    // 1 2 3 9 10 11 12 13 14 4
    printf("\n");

    printf("删除操作:\n");
    DeleteSqList(list, 2);
    DisplaySqList(list);
    // 1 2 3 9 10 12 13 14 4
    printf("\n");

    printf("查询操作:\n");
    if (SelectSqList(list, 3, &mydata) == -1)
    {
        printf("位置%d没有数据\n", 3);
        return -1;
    }
    else
    {
        printf("位置%d的数据为：%d\n", 3, mydata);
    }
    printf("\n");

    printf("修改操作:\n");
    UpdateSqList(list, 3, 99);
    DisplaySqList(list);
    // 1 2 3 99 10 12 13 14 4
    printf("\n");

    LenthSqList(list);
    printf("\n");

    int result[N + 1];
    FindSqList(list, result, 3);
    for (int i = 0; i < sizeof(result); i++)
    {
        if (result[i] == -1)
        {
            break;
        }
        printf("数据:%d在表中%d位置\n", 3, result[i]);
    }
    printf("ok\n");

    DeleteDataList(list, 3);
    DisplaySqList(list);
    LenthSqList(list);

    // ClearSqList(list);
    // LenthSqList(list);

    // DestorySqList(&list);

    return 0;
}
/*************************************************************************
> File Name: sqlist.c
> Author: hzy
> Created Time: 2023-07-25 16:30:10
> Description:功能实现函数
***************s*********************************************************/
#include "config.h"

// 创建顺序表
sqList *CreateSqList(/*int mysize*/)
{
    sqList *list = malloc(sizeof(sqList));
    if (list == NULL)
    {
        printf("创建顺序表空间失败");
        return list;
    }

    memset(list, 0, sizeof(sqList));

    // // 动态开辟空间
    // list->data = malloc(sizeof(data_t) * mysize);
    // if (list->data == NULL)
    // {
    //     free(list);
    //     return NULL;
    // }

    // list->size = mysize;

    list->last = 0;

    return list;
}

// 插入
int InserSqList(sqList *list, int local, data_t mydata)
{
    isEmptySqList(list);

    // 判断最后一个元素是否为表的最大长度
    if (list->last == N - 1)
    {
        return -1;
    }

    // 判断插入位置是否 0 < local < list->last
    if (local < 0 || local > list->last + 1)
    {
        printf("插入位置不符合规定\n");
        return -1;
    }

    // 将local后位置的元素依次往后移动
    for (int i = list->last; i >= local; i--)
    {
        list->data[i] = list->data[i - 1];
    }

    // 将我的数据存到表中指定位置
    list->data[local - 1] = mydata;

    // 表长度增加
    list->last++;

    return local;
}

// 删除指定位置的值
int DeleteSqList(sqList *list, int local)
{
    isEmptySqList(list);

    if (local < 0 || local > list->last)
    {
        return -1;
    }

    for (int i = local; i < list->last; i++)
    {
        list->data[i - 1] = list->data[i];
    }

    list->last--;

    return 0;
}

// 删除指定的值
int DeleteDataList(sqList *list, data_t mydata)
{
    isEmptySqList(list);

    int i = 0;
    while (i < list->last)
    {
        if (list->data[i] == mydata)
        {
            for (int j = i; j < list->last-1; j++)
            {
                list->data[j] = list->data[j+1];
            }
            list->last--;
        }
        i++;
    }
    return 0;
}

// 查询指定位置的元素数据
int SelectSqList(sqList *list, int local, data_t *mydata)
{
    // 判断顺序表是否为空
    isEmptySqList(list);
    // 判断local的位置是否满足 0 <= local <= last
    if (local < 0 || local > list->last)
    {
        return -1;
    }
    // 读取查询到的数据信息
    *mydata = list->data[local - 1];

    return 0;
}

// 查询一个数据并同时返回数据的位置
int FindSqList(sqList *list, int *local, data_t mydata)
{
    // 判断顺序表是否为空
    isEmptySqList(list);
    //
    int x = 0;
    while (x < list->last)
    {
        if (list->data[x] == mydata)
        {
            *local++ = x + 1;
        }
        x++;
    }
    *local = -1;

    return 0;
}

int UpdateSqList(sqList *list, int local, data_t mydata)
{
    // 判断顺序表是否为空
    isEmptySqList(list);
    // 判断local的位置是否满足 0 <= local <= last
    if (local < 0 || local > list->last)
    {
        return -1;
    }
    // 修改指定位置的数据
    list->data[local - 1] = mydata;

    return 0;
}

// 判断顺序表是否为空表
int isEmptySqList(sqList *list)
{
    if (list->last == 0)
    {
        printf("顺序表为空\n");
    }
    return list->last == 0;
}

// 判断顺序表是否为满表
int isFullSqList(sqList *list)
{
    if (list->last == N)
    {
        printf("顺序表已满\n");
    }

    return (list->last == N);
}

// 判断顺序表的长度
int LenthSqList(sqList *list)
{
    printf("顺序表的长度为：%d\n", (list->last));
    return (list->last);
}

// 清空表中所有数据
int ClearSqList(sqList *list)
{
    printf("顺序表中的数据已被清空\n");
    return (list->last = 0);
}

// 显示顺序表中所有数据记录
int DisplaySqList(sqList *list)
{
    if (list == NULL)
    {
        printf("顺序表为空");
        return -1;
    }

    printf("顺序表中的数据为：");
    for (int i = 0; i < list->last; i++)
    {
        printf("%-4d", list->data[i]);
    }
    printf("\n");
}

// 释放顺序表存储空间
void DestorySqList(sqList **list)
{
    free(*list);
    *list = NULL;
    printf("顺序表的空间已被释放\n");
}
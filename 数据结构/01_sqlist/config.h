/*************************************************************************
> File Name: config.h
> Author: hzy
> Created Time: 2023-07-25 16:19:21
> Description:头文件
************************************************************************/
#ifndef _CONFIG_H_
#define _CONFIG_H_

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define N 10
/*
typedef struct Stu
{
    int num;
    char name[32];
    int age;
} data_t;
*/
typedef int data_t;

typedef struct sqList
{
    data_t data[N];
    // int size;
    int last;
} sqList;

// 声明顺序表的运算函数
sqList *CreateSqList(/*int mysize*/);                      // 创建顺序表
int InserSqList(sqList *list, int local, data_t mydata);   // 插入
int DeleteSqList(sqList *list, int local);                 // 删除
int SelectSqList(sqList *list, int local, data_t *mydata); // 查找
int UpdateSqList(sqList *list, int local, data_t mydata);  // 修改
int isEmptySqList(sqList *list);                           // 判断顺序表是否为空表
int isFullSqList(sqList *list);                            // 判断顺序表是否为满表
int LenthSqList(sqList *list);                             // 判断顺序表的长度
int ClearSqList(sqList *list);                             // 清空表中所有数据
int DisplaySqList(sqList *list);                           // 显示顺序表中所有数据记录
void DestorySqList(sqList **list);                         // 释放*list整个空间并置为NULL
int FindSqList(sqList *list,int *local ,data_t mydata);               // 查询一个数据并同时返回数据的位置
int DeleteDataList(sqList *list, data_t mydata);

#endif
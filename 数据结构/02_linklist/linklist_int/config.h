#ifndef _CONFIG_H_
#define _CONFIG_H_

/* 结点数据域类型：可以根据实际应用需要存储的数据记录进行抽象 */
#if 0
typedef struct Stu {
	int num;
	char name[32];
	int score;
} data_t;
#else 
typedef int data_t;
#endif

/* 整个结点数据类型： */
typedef struct node {
	data_t data;		/* 当前结点数据域：存储当前结点的数据 */
    struct node *next;	/* 当前结点指针域：存储下一个结点空间的起始地址 */
} node_t;

node_t *CreateLinkList(void);
int InsertLinkList(node_t *head, int local, data_t mydata);
void DisplayLinkList(node_t *head);
int DeleteLinkList(node_t *head, int local);
int SelectLinkList(node_t *head, int local, data_t *mydata);
int UpdateLinkList(node_t *head, int local, data_t newdata);
int isEmptyLinkList(node_t *head);
int isFullLinkList(node_t *head);
int LengthLinkList(node_t *head);
void ClearLinkList(node_t *head);
void DestoryLinkList(node_t **head);
void ReverseLinkList(node_t *head);
void SortLinkList(node_t *head);
node_t * MergeLinkList(node_t **head1, node_t **head2);
#endif
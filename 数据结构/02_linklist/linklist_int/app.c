#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "config.h"

int main()
{
	int i;
	node_t *head;
	node_t *head2;
	data_t mydata;
	
	
	head = CreateLinkList();
	if (head == NULL)
		return -1;
		
	i = 5;
	while(--i) {
		if (-1 == InsertLinkList(head, 0, i)) {
			printf("insert %d error\n", i);
			break;
		}
	}
	/* 1 2 3 4 */
	
	i = 15;
	while(--i) {
		if (-1 == InsertLinkList(head, 3, i)) {
			printf("insert %d error\n", i);
			break;
		}
	}
	/* 1 2 3 1 2 3 4 5 6 7 8 9 10 11 12 13 14 4 */
	
	DisplayLinkList(head);
	if (-1 == DeleteLinkList(head, 17))  {
		printf("delete local = 5 error\n");
	}
	DisplayLinkList(head);
	
	if (-1 == SelectLinkList(head, 0, &mydata)) {
		printf("local = 0 no data\n");
	} else {
		printf("0->%d\n", mydata);
	}
	
	if (-1 == SelectLinkList(head, 17, &mydata)) {
		printf("local = 17 no data\n");
	} else {
		printf("17->%d\n", mydata);
	}
	
	if (-1 == SelectLinkList(head, 16, &mydata)) {
		printf("local = 16 no data\n");
	} else {
		printf("16->%d\n", mydata);
	}
	
	if (-1 == UpdateLinkList(head, 17, 117)) {
		printf("local = 17 update error\n");
	}
	
	if (-1 == UpdateLinkList(head, 10, 717)) {
		printf("local = 10 update error\n");
	}
	DisplayLinkList(head);	
	ReverseLinkList(head);
	DisplayLinkList(head);
	SortLinkList(head);
	printf("head: \n");
	DisplayLinkList(head);
	
}
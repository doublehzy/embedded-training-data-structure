/*************************************************************************
> File Name: main.c
> Author: hzy
> Created Time: 2023-07-26 19:03:07
> Description:
************************************************************************/
#include "config.h"

int main(int argc, char const *argv[])
{
    int k = 0, x = 0;
    int my_age, my_id;
    char my_name[32];

    node_t *head = CreateLinkList();
    //判断链表是否为空链表
    isEmptyLinkList(head);

    printf("请输入插入数据的位置：");
    scanf("%d", &k);
    printf("输入需要插入x条数据：");
    scanf("%d", &x);
    for (int i = 0; i < x; i++)
    {
        printf("输入ID:");
        scanf("%d", &my_id);
        printf("输入姓名:");
        scanf("%s", my_name);
        printf("输入年龄:");
        scanf("%d", &my_age);
        InserLinkList(head, k, my_id, my_name, my_age);
    }
    DisplayLinkList(head);

    printf("请输入查询数据的位置：");
    scanf("%d", &k);
    if (SelectLinkList(head, k, &my_id, my_name, &my_age) == -1)
    {
        printf("位置%d上没有数据", k);
    }
    else
    {
        printf("位置%d上的数据为：", k);
        printf("学号:%-6d 姓名:%-4s 年龄:%-2d\n", my_id, my_name, my_age);
    }

    printf("请输入修改数据的位置：");
    scanf("%d", &k);
    printf("输入ID:");
    scanf("%d", &my_id);
    printf("输入姓名:");
    scanf("%s", my_name);
    printf("输入年龄:");
    scanf("%d", &my_age);
    UpdateLinkList(head, k, my_id, my_name, my_age);
    DisplayLinkList(head);

    int len = 0;
    len = LengthLinkList(head);
    printf("链表的长度为%d\n",len);

    printf("请输入删除数据的位置：");
    scanf("%d", &k);
    DeleteLinkList(head,k);
    DisplayLinkList(head);

    len = LengthLinkList(head);
    printf("链表的长度为%d\n",len);

    return 0;
}
/*************************************************************************
> File Name: linklist.c
> Author: hzy
> Created Time: 2023-07-26 17:23:25
> Description:
************************************************************************/
#include "config.h"

// 创建链表，开辟结点存储空间
node_t *CreateLinkList(void)
{
    // 开辟头结点存储空间
    node_t *head = malloc(sizeof(node_t));
    if (head == NULL)
    {
        return NULL;
    }

    memset(head, 0, sizeof(node_t));
    head->next = NULL;

    return head;
}

// 在指定结点插入数据
int InserLinkList(node_t *head, int local, int my_id, char *my_name, int my_age)
{
    int i = 0;
    node_t *p = head;
    node_t *q;

    if (local < 0)
        return -1;

    for (i = 0; i < local && p != NULL; i++)
    {
        p = p->next;
    }
    if (p == NULL)
    {
        return -1;
    }
    q = malloc(sizeof(node_t));
    if (q == NULL)
    {
        return -1;
    }
    memset(q, 0, sizeof(node_t));

    q->next = p->next;
    p->next = q;

    q->data.id = my_id;
    strcpy(q->data.name, my_name);
    q->data.age = my_age;

    return 0;
}

// 删除链表指定结点数据
int DeleteLinkList(node_t *head, int local)
{
    int i = 0;
    node_t *p = head;
    node_t *q;

    // 判断链表是否为空表
    if (head->next == NULL)
    {
        return -1;
    }

    for (i = 0; i < local && p->next != NULL; i++)
    {
        p = p->next;
    }
    if (p->next == NULL)
    {
        return -1;
    }

    q = p->next;
    p->next = q->next;
    free(q);

    return 0;
}

// 查询链表指定结点数据
int SelectLinkList(node_t *head, int local, int *my_id, char *my_name, int *my_age)
{
    node_t *p;

    // 判断链表是否为空表
    if (head->next == NULL)
    {
        return -1;
    }

    // 找到指定位置的结点
    p = head->next;
    for (int i = 0; i < local && p != NULL; i++)
    {
        p = p->next;
    }
    if (p == NULL)
    {
        return -1;
    }

    *my_id = p->data.id;
    strcpy(my_name, p->data.name);
    *my_age = p->data.age;

    return 0;
}

// 更新链表指定结点数据
int UpdateLinkList(node_t *head, int local, int my_id, char *my_name, int my_age)
{
    node_t *p;

    if (head->next == NULL)
    {
        return -1;
    }

    // 找到指定位置的结点
    p = head->next;
    for (int i = 0; i < local && p != NULL; i++)
    {
        p = p->next;
    }
    if (p == NULL)
    {
        return -1;
    }

    // 修改结点的数据域
    p->data.id = my_id;
    strcpy(p->data.name, my_name);
    p->data.age = my_age;

    return 0;
}

// 遍历链表所以结点数据
void DisplayLinkList(node_t *head)
{
    node_t *p = head->next;

    if (p == NULL)
    {
        printf("链表中数据为空");
    }

    while (p != NULL)
    {
        printf("ID:%-6d ", p->data.id);
        printf("姓名:%-4s ", p->data.name);
        printf("年龄:%-2d", p->data.age);
        p = p->next;
    }
    printf("\n");
}

// 判断链表是否为空
int isEmptyLinkList(node_t *head)
{
    if (head == NULL)
        printf("链表为空\n");
    return 0;
}

// 判断链表是否为满
int isFullLinkList(node_t *head)
{
    return 0; // 链表不可为满表,返回0表示false
}

// 计算链表的长度
int LengthLinkList(node_t *head)
{
    int len = 0;
    node_t *p = head->next;

    while (p != NULL)
    {
        len++;
        p = p->next;
    }

    return len;
}

// 清空链表数据
void ClearLinkList(node_t *head)
{
    node_t *q;
    node_t *p = head->next;

    while (p != NULL)
    {
        q = p;
        p = p->next;
        free(q);
    }
    head->next = NULL;
}

// 释放链表的空间
void DestoryLinkList(node_t **head)
{
    node_t *q;
    node_t *p = *head;

    while (p != NULL)
    {
        q = p;
        p = p->next;
        free(q);
    }
    *head = NULL;
}

// 反向链表数据
void ReverseLinkList(node_t *head)
{
    node_t *q;
    node_t *p = head->next; // p是用来遍历整个链表中的有效数据结点
    head->next = NULL;      // 头结点的指针域设置为NULL

    // 实现倒叙运算处理
    while (p != NULL)
    {
        q = p;       // 遍历到的结点p赋值给运算结点q
        p = p->next; // 遍历到下一个结点p
        // 头部插入：实现后遍历到的结点插入到先遍历到的结点之前
        q->next = head->next;
        head->next = q;
    }
}
/*
//按学号由小到大排序
void SortLinkList(node_t *head)
{
    node_t *p;
    node_t *q;
    node_t *r;
    data_t mydata;

    if (head->next == NULL)
    {
        return;
    }

    p = head->next->next;
    head->next->next = NULL;

    while (p != NULL)
    {
        q = p;
        p = p->next;

        r = head;
        while (r->next != NULL) // 判断r的下一个结点是否存在
        {
            if (r->next > q->)
            {
                break;
            }
            r = r->next;
        }
        // 在结点r后插入待排序的结点q
        q->next = r->next;
        r->next = q;
    }
}
*/
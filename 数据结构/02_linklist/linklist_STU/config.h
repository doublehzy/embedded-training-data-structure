/*************************************************************************
> File Name: config.h
> Author: hzy
> Created Time: 2023-07-26 17:17:39
> Description:
************************************************************************/
#ifndef _CONFIG_H_
#define _CONFIG_H_

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

typedef struct stu
{
    int id;
    char name[32];
    int age;
} data_t;

// typedef int data_t;

typedef struct node
{
    // 数据域
    data_t data;
    // 指针域
    struct node *next;
} node_t;

node_t *CreateLinkList(void);                                                         // 创建链表，开辟结点存储空间
int InserLinkList(node_t *head, int local, int my_id, char *my_name, int my_age);     // 在指定结点插入数据
int DeleteLinkList(node_t *head, int local);                                          // 删除链表指定结点数据
int SelectLinkList(node_t *head, int local, int *my_id, char *my_name, int *my_age); // 查询链表指定结点数据
int UpdateLinkList(node_t *head, int local, int my_id, char *my_name, int my_age);    // 更新链表指定结点数据
void DisplayLinkList(node_t *head);                                                   // 遍历链表所以结点数据
int isEmptyLinkList(node_t *head);                                                    // 判断链表是否为空
int isFullLinkList(node_t *head);                                                     // 判断链表是否为满
int LengthLinkList(node_t *head);                                                     // 计算链表的长度
void ClearLinkList(node_t *head);                                                     // 清空链表数据
void DestoryLinkList(node_t **head);                                                  // 释放链表的空间
void ReverseLinkList(node_t *head);                                                   // 反向链表数据
//void SortLinkList(node_t *head);                                                    // 由小到大排序

#endif
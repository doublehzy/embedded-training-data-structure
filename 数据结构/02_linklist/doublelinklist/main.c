/*************************************************************************
> File Name: main.c
> Author: hzy
> Created Time: 2023-07-29 10:43:18
> Description: 
************************************************************************/
#include "linklist.h"
 
int main(int argc, char const *argv[])
{
    int k = 0, x = 0;
    int my_age, my_id;
    char my_name[32];

    node_t *head = CreateLinkList();

    printf("请输入插入数据的位置：");
    scanf("%d", &k);
    printf("输入需要插入x条数据：");
    scanf("%d", &x);
    for (int i = 0; i < x; i++)
    {
        printf("输入第%d条信息:\n",i);
        printf("输入ID:");
        scanf("%d", &my_id);
        printf("输入姓名:");
        scanf("%s", my_name);
        printf("输入年龄:");
        scanf("%d", &my_age);
        InserLinkList(head, k, my_id, my_name, my_age);
    }
    DisplayLinklist(head);
    
    
    return 0;
}
/*************************************************************************
> File Name: linklist.h
> Author: hzy
> Created Time: 2023-07-29 10:42:42
> Description:
************************************************************************/
#ifndef _LINKLIST_H_
#define _LINKLIST_H_

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

typedef struct data
{
    int id;
    char name[20];
    int age;
} data_t;

typedef struct node
{
    // 数据域
    data_t data;
    // 指针域
    struct node *pre;  // 存储前驱结点
    struct node *next; // 存储后继结点
}node_t;


node_t *CreateLinkList(void);
void DisplayLinklist(node_t *head);
int InserLinkList(node_t *head, int local, int my_id, char *my_name, int my_age);



#endif
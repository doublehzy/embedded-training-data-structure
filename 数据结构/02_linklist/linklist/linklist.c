#include "linklist.h"

node_t *CreateLinkList(void)
{
    node_t *head;

    head = malloc(sizeof(node_t));
    if (head == NULL)
    {
        return NULL;
    }
    memset(head, 0, sizeof(node_t));

    head->next = NULL;

    return head;
}

// 打印链表中的数据
int PrintfLinkList(node_t *head)
{
    node_t *p = head->next;

    if (p == NULL)
    {
        printf("链表数据为空\n");
        return -1;
    }

    while (p != NULL)
    {
        printf("%-4d", p->data);
        p = p->next;
    }
    printf("\n");
}

// 计算链表的长度
int LenthLinkList(node_t *head)
{
    node_t *p = head;
    int len = 0;

    if (p == NULL)
    {
        perror("链表不存在\n");
        return -1;
    }

    while (p->next != NULL)
    {
        len++;
        p = p->next;
    }

    return len;
}

// 判断链表是否为空
int isEmptyLinkList(node_t *head)
{
    if (head == NULL)
    {
        perror("链表不存在\n");
        return -1;
    }

    return (head->next == NULL) ? -1 : 0;
}

// 在指定位置插入
int InsertLinkList(node_t *head, int local, data_t mydata)
{
    node_t *p = head;

    /* 判断插入位置是否满足条件：1 < local < 链表最大长度+1 */
    if (local < 1 || local > LenthLinkList(head) + 1)
    {
        perror("插入位置不合法\n");
        return -1;
    }
    /* 找到需要插入结点的前一个结点 */
    for (int i = 1; i < local; i++)
    {
        p = p->next;
    }

    node_t *q = malloc(sizeof(node_t));
    if (q == NULL)
    {
        perror("插入结点创建空间失败\n");
        return -1;
    }
    memset(q, 0, sizeof(node_t));

    q->data = mydata;

    q->next = p->next;
    p->next = q;

    return 0;
}
// 尾插法
int InsertLinkList_end(node_t *head, data_t mydata)
{
    node_t *p = head;

    if (p == NULL)
    {
        perror("链表不存在\n");
        return -1;
    }

    node_t *q = malloc(sizeof(node_t));
    if (q == NULL)
    {
        perror("插入结点创建空间失败\n");
        return -1;
    }
    memset(q, 0, sizeof(node_t));

    q->data = mydata;

    while (p->next != NULL)
    {
        p = p->next;
    }

    q->next = p->next;
    p->next = q;

    return 0;
}

// 头插法
int InsertLinkList_head(node_t *head, data_t mydata)
{
    node_t *p = head;

    if (p == NULL)
    {
        perror("链表不存在\n");
        return -1;
    }

    node_t *q = malloc(sizeof(node_t));
    if (q == NULL)
    {
        perror("插入结点创建空间失败\n");
        return -1;
    }
    memset(q, 0, sizeof(node_t));

    q->data = mydata;

    q->next = p->next;
    p->next = q;

    return 0;
}

// 删除
int DeleteLinkList(node_t *head, int local, data_t *mydata)
{
    if (local < 1 || local > LenthLinkList(head) || isEmptyLinkList(head))
    {
        return -1;
    }

    node_t *p = head;
    for (int i = 1; i < local; i++)
    {
        p = p->next;
    }

    *mydata = p->next->data;

    node_t *q = p->next;
    p->next = q->next;
    free(q);

    return 0;
}

// 修改指定位置的数据
int UpdateLinkList(node_t *head, int local, data_t mydata)
{
    if (local < 1 || local > LenthLinkList(head) || isEmptyLinkList(head))
    {
        return -1;
    }

    node_t *p = SelectLinkList(head, local);

    p->data = mydata;

    return 0;
}

// 查询指定位置的数据
node_t *SelectLinkList(node_t *head, int local)
{
    if (isEmptyLinkList(head) || local < 1 || local > LenthLinkList(head))
    {
        perror("查询失败\n");
        return NULL;
    }

    node_t *p = head->next;
    for (int i = 1; i < local; i++)
    {
        p = p->next;
    }

    return p;
}

// 清空链表的数据
int ClearLinkList(node_t *head)
{
    if (head == NULL)
    {
        perror("链表不存在\n");
        return -1;
    }
    head->next = NULL;

    return 0;
}

//倒叙
int ReverserLinkList(node_t *head)
{
    node_t *p = head->next;

    if (p == NULL)
    {
        printf("链表数据为空\n");
        return -1;
    }

    head->next = NULL;
    while (p != NULL)
    {
        InsertLinkList_head(head,p->data);
        node_t *q = p;
        p = p->next;
        free(q);
    }

    return 0;
}
/*************************************************************************
> File Name: listlist.h
> Author: hzy
> Created Time: 2023-08-01 16:17:59
> Description:
************************************************************************/
#ifndef _LINKLIST_H_
#define _LINKLIST_H_

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

typedef int data_t;

typedef struct node
{
    // 数据域
    data_t data;
    // 指针域
    struct node *next;
}node_t;

node_t *CreateLinkList(void);
int isEmptyLinkList(node_t *head);
int LenthLinkList(node_t *head);
node_t *SelectLinkList(node_t *head, int local);

int PrintfLinkList(node_t *head);
int InsertLinkList(node_t *head, int local, data_t mydata);        //指定位置插入
int InsertLinkList_end(node_t *head, data_t mydata);               //尾插法
int InsertLinkList_head(node_t *head, data_t mydata);              //头插法
int DeleteLinkList(node_t *head, int local,data_t *data);
int UpdateLinkList(node_t *head, int local, data_t mydata);
int ClearLinkList(node_t *head);
int ReverserLinkList(node_t *head);

#endif
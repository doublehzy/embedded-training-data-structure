/*************************************************************************
> File Name: main.c
> Author: hzy
> Created Time: 2023-08-01 16:17:37
> Description:
************************************************************************/
#include "linklist.h"

int main(int argc, char const *argv[])
{
    node_t *head;
    int len = 0;

    head = CreateLinkList();
    for (int i = 1; i <= 5; i++)
    {
        InsertLinkList(head, 1, i);
    }
    PrintfLinkList(head);
    len = LenthLinkList(head);
    printf("链表的长度为：%d\n", len);
    for (int i = 10; i >= 5; i--)
    {
        InsertLinkList(head, 6, i);
    }
    PrintfLinkList(head);
    InsertLinkList(head, 1, 99);
    PrintfLinkList(head);
    // for (int i = 1; i <= 5; i++)
    // {
    //     InsertLinkList_end(head, i);
    // }
    // PrintfLinkList(head);
    // for (int i = 20; i >= 15; i--)
    // {
    //     InsertLinkList_head(head, i);
    // }
    // PrintfLinkList(head);

    len = LenthLinkList(head);
    printf("链表的长度为：%d\n", len);

    data_t mydata;
    if (DeleteLinkList(head, 1, &mydata) == -1)
    {
        printf("删除失败\n");
        return -1;
    }
    printf("数据：%d删除成功\n", mydata);
    PrintfLinkList(head);

    node_t *p = SelectLinkList(head, 1);
    if (p != NULL)
    printf("位置%d的数据为%d\n", 1, p->data);

    if (UpdateLinkList(head,11,88) == -1)
    {
        printf("修改失败\n");
        return -1;
    }
    printf("修改成功\n");
    PrintfLinkList(head);

    ReverserLinkList(head);
    PrintfLinkList(head);

    ClearLinkList(head);
    PrintfLinkList(head);
    

    return 0;
}